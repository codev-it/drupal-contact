<?php

namespace Drupal\Tests\codev_contact\Functional\Helper;

use Drupal\codev_contact\Helper\Location;
use Drupal\Tests\codev_contact\Functional\FunctionalTestBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: StandardTest.php
 * .
 */

/**
 * Class LocationTest.
 *
 * @package      Drupal\Tests\codev_contact\Functional\Helper
 *
 * @group        codev_contact
 *
 * @noinspection PhpUnused
 */
class LocationTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Test location helper.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testLocation() {
    // Create location
    $location = Location::createLocationParagraph($this->location);
    $location->save();
    $this->assertLocation($location, $this->location);

    $location2 = Location::createLocationParagraph($this->location2);
    $location2->save();
    $this->assertLocation($location2, $this->location2);

    // Create location paragraph entity
    $node = $this->drupalCreateNode(['type' => 'contact']);
    $this->assertTrue(Location::setLocationToContactNode($node, $this->location));

    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    $field_location = $node->get('field_location')->referencedEntities() ?: [];
    $this->assertTrue(count($field_location) === 1);
    $this->assertLocation($field_location[0], $this->location);

    $node_multiple = $this->drupalCreateNode(['type' => 'contact']);
    $this->assertTrue(Location::setLocationToContactNode($node_multiple, $this->locationMultiple));

    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    $field_location = $node_multiple->get('field_location')
      ->referencedEntities() ?: [];
    $this->assertTrue(count($field_location) === 2);
    $this->assertLocation($field_location[0], $this->location);
    $this->assertLocation($field_location[1], $this->location2);

    // Append location paragraph entity
    $this->assertTrue(Location::appendLocationToContactNode($node, $this->location2));
    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    $field_location = $node->get('field_location')->referencedEntities() ?: [];
    $this->assertTrue(count($field_location) === 2);
    $this->assertLocation($field_location[0], $this->location);
    $this->assertLocation($field_location[1], $this->location2);

    $this->assertTrue(Location::appendLocationToContactNode($node_multiple, $this->locationMultiple));
    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    $field_location = $node_multiple->get('field_location')
      ->referencedEntities() ?: [];
    $this->assertTrue(count($field_location) === 4);
    $this->assertLocation($field_location[0], $this->location);
    $this->assertLocation($field_location[1], $this->location2);
    $this->assertLocation($field_location[2], $this->location);
    $this->assertLocation($field_location[3], $this->location2);

    // Test location clean address builder
    $location_accepts = $this->location;
    foreach (['phone', 'fax', 'mail'] as $item) {
      if (!empty($location_accepts[$item]) && !is_array($location_accepts[$item])) {
        $location_accepts[$item] = [$location_accepts[$item]];
      }
    }
    $this->assertEquals($location_accepts, Location::buildCleanAddress($location));
    $this->assertEquals($this->location2, Location::buildCleanAddress($location2));

    // Test get location from by node.
    $node_locations = Location::getLocationFromContactNode($node);
    foreach ($node_locations as $location_id => $location) {
      foreach ($location as $key => $val) {
        if (!array_key_exists($key, $this->location)) {
          unset($node_locations[$location_id][$key]);
        }
      }
    }
    $this->assertEquals([$location_accepts, $this->location2], $node_locations);
  }

}
