<?php

namespace Drupal\Tests\codev_contact\Functional\Helper;

use Drupal;
use Drupal\codev_contact\Helper\Contact;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\Tests\codev_contact\Functional\FunctionalTestBase;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: StandardTest.php
 * .
 */

/**
 * Class LocationTest.
 *
 * @package      Drupal\Tests\codev_contact\Functional\Helper
 *
 * @group        codev_contact
 *
 * @noinspection PhpUnused
 */
class ContactTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $nodeTitle;

  /**
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $menuLinkTitle;

  protected function setUp(): void {
    parent::setUp();
    $this->entityTypeManager = Drupal::entityTypeManager();

    $this->nodeTitle = t('Contact');
    $this->menuLinkTitle = t('Contact menu link');
  }

  /**
   * Test contact helper.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testContact() {
    // Create contact node without field values.
    $node = Contact::createNode($this->nodeTitle);
    $this->assertTrue($node instanceof Node);
    $this->assertTrue(!$node->isNew());

    $this->assertTrue($node->hasField('uid'));
    $this->assertTrue(!$node->get('uid')->isEmpty());
    $uid = $node->get('uid')->first()->getValue()['target_id'];
    $this->assertEquals(1, $uid);

    $this->assertTrue($node->hasField('body'));
    $this->assertTrue($node->get('body')->isEmpty());

    $this->assertTrue($node->hasField('field_location'));
    $this->assertTrue($node->get('field_location')->isEmpty());

    $menu_link = $this->getMenuLinkByNode($node);
    $this->assertTrue($menu_link instanceof MenuLinkContent);
    $this->assertEquals((string) $this->nodeTitle, $menu_link->label());

    // Create contact node with field values.
    $node = Contact::createNode($this->nodeTitle, [
      'body'      => '<h2>Content area!</h2>',
      'location'  => $this->location,
      'menu_link' => [
        'title' => $this->menuLinkTitle,
      ],
    ]);
    $this->assertTrue($node instanceof Node);

    $this->assertTrue($node->hasField('body'));
    $this->assertTrue(!$node->get('body')->isEmpty());
    $body_val = $node->get('body')->first()->getValue()['value'];
    $this->assertEquals('<h2>Content area!</h2>', $body_val);

    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    $field_location = $node->get('field_location')->referencedEntities() ?: [];
    $this->assertTrue(count($field_location) === 1);
    $this->assertLocation($field_location[0], $this->location);

    $menu_link = $this->getMenuLinkByNode($node);
    $this->assertTrue($menu_link instanceof MenuLinkContent);
    $this->assertEquals((string) $this->menuLinkTitle, $menu_link->label());

    // Create contact node without link.
    $node = Contact::createNode($this->nodeTitle, ['menu_link' => FALSE]);
    $this->assertTrue($node instanceof Node);
    $this->assertTrue(!$node->isNew());

    $this->assertTrue($node->hasField('body'));
    $this->assertTrue($node->get('body')->isEmpty());

    $menu_link = $this->getMenuLinkByNode($node);
    $this->assertEquals(NULL, $menu_link);

    // Create contact node with given user.
    $user = $this->drupalCreateUser();
    $node = Contact::createNode($this->nodeTitle, [], $user);
    $this->assertTrue($node instanceof Node);

    $this->assertTrue($node->hasField('uid'));
    $this->assertTrue(!$node->get('uid')->isEmpty());
    $uid = $node->get('uid')->first()->getValue()['target_id'];
    $this->assertEquals($user->id(), $uid);
  }

  /**
   * Get the menu link by node object.
   *
   * @param \Drupal\node\Entity\Node $node
   *
   * @return \Drupal\menu_link_content\Entity\MenuLinkContent|null
   */
  private function getMenuLinkByNode(Node $node): ?MenuLinkContent {
    try {
      /** @var \Drupal\menu_link_content\MenuLinkContentStorage $menu_link_storage */
      $menu_link_storage = $this->entityTypeManager->getStorage('menu_link_content');
      /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $menu_link */
      $menu_links = $menu_link_storage->loadByProperties([
        'link' => ['uri' => 'internal:/node/' . $node->id()],
      ]) ?: [];
      $menu_link = end($menu_links);
      return !empty($menu_link) ? $menu_link : NULL;
    } catch (Exception $exception) {
      return NULL;
    }
  }

}
