<?php

namespace Drupal\Tests\codev_contact\Functional;

use Drupal\codev_contact\Helper\Location;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: CodevContactTest.php
 * .
 */

/**
 * Class CodevContactTest.
 *
 * Tests installation module expectations.
 *
 * @package      Drupal\Tests\codev_contact\Functional
 *
 * @group        codev_contact
 *
 * @noinspection PhpUnused
 */
class CodevContactTest extends FunctionalTestBase {

  /**
   * The admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser();
  }

  /**
   * Tests the functionality.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testCodevContact() {
    // Defaults
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('node/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists(t('Contact'));

    // Node contact
    $this->drupalGet('node/add/contact');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Title'));
    $this->assertSession()->pageTextContains(t('Body'));
    $node = $this->drupalCreateNode(['type' => 'contact']);

    // Check block form and field exist.
    $this->drupalGet('admin/structure/block/add/location');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Locations'));

    // Create location paragraph entity for location block
    $this->assertTrue(Location::setLocationToContactNode($node, $this->location));

    $this->assertTrue(Location::appendLocationToContactNode($node, array_map(
      function ($item) { return $item != 'AT' ? $item . '2' : $item; },
      $this->location)));

    // Create location block
    $this->submitForm([
      'id'                      => 'location',
      'settings[label]'         => t('Location'),
      'settings[label_display]' => TRUE,
      'settings[locations]'     => $node->id(),
    ], t('Save block'));
    $this->drupalPlaceBlock('location', [
      'label'     => t('Location'),
      'locations' => [$node->id()],
    ]);

    // Anonymous
    $this->drupalLogout();
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);

    // Check block content
    foreach ($this->location as $item) {
      $this->assertSession()->pageTextContains($item);
    }

    foreach ($this->location as $key => $item) {
      if ($key !== 'country_code') {
        $this->assertSession()->pageTextContains($item . '2');
      }
      else {
        $this->assertSession()->pageTextContains($item);
      }
    }
  }

}
