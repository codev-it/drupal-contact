<?php
/** @noinspection PhpHierarchyChecksInspection */

namespace Drupal\Tests\codev_contact\Functional;

use Drupal\codev_utils\Helper\Utils;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\RequirementsPageTrait;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: FunctionalTestBase.php
 * .
 */

/**
 * Class FunctionalTestBase.
 *
 * Default functional test base settings.
 *
 * @package      Drupal\Tests\codev_contact\Functional
 *
 * @group        codev_contact
 *
 * @noinspection PhpUnused
 */
abstract class FunctionalTestBase extends BrowserTestBase {

  use SchemaCheckTestTrait;
  use RequirementsPageTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_contact',
  ];

  /**
   * The correct location data.
   *
   * @var array
   */
  protected array $location;

  /**
   * The correct location data.
   *
   * @var array
   */
  protected array $location2;

  /**
   * The correct location data.
   *
   * @var array
   */
  protected array $locationMultiple;

  /**
   * An array of config object names that are excluded from schema checking.
   *
   * @var string[]
   */
  protected static $configSchemaCheckerExclusions = [
    'field.storage.paragraph.field_view',
    'field.field.paragraph.view.field_view',
    'core.entity_view_display.paragraph.view.default',
    'core.entity_form_display.block_content.basic.default',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->location = [
      'organization'  => 'Organization',
      'given_name'    => 'Given name',
      'family_name'   => 'Family Name',
      'country_code'  => 'AT',
      'locality'      => 'Locality',
      'postal_code'   => '0000',
      'address_line1' => 'Address line 1',
      'phone'         => '000000000000',
      'fax'           => '000000000000',
      'mail'          => 'example@example.at',
    ];

    $this->location2 = $this->location;
    $this->location2['phone'] = ['000000000000', '000000000000'];
    $this->location2['fax'] = ['000000000000', '000000000000'];
    $this->location2['mail'] = ['example@example.at', 'example@example.at'];

    $this->locationMultiple = [$this->location, $this->location2];
  }

  /**
   * Test given paragraph location.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph|null $location
   * @param array                                    $data
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function assertLocation(?Paragraph $location, array $data = []) {
    $this->assertTrue($location instanceof Paragraph);
    $this->assertTrue($location->hasField('field_location'));
    $this->assertTrue($location->hasField('field_phone'));
    $this->assertTrue($location->hasField('field_fax'));
    $this->assertTrue($location->hasField('field_e_mail'));
    $this->assertTrue(!$location->get('field_location')->isEmpty());
    $this->assertTrue(!$location->get('field_phone')->isEmpty());
    $this->assertTrue(!$location->get('field_fax')->isEmpty());
    $this->assertTrue(!$location->get('field_e_mail')->isEmpty());

    $field_location = $location->get('field_location')->first()->getValue();
    $accepted = [
      'organization'  => Utils::getArrayValue('organization', $data),
      'given_name'    => Utils::getArrayValue('given_name', $data),
      'family_name'   => Utils::getArrayValue('family_name', $data),
      'country_code'  => Utils::getArrayValue('country_code', $data),
      'locality'      => Utils::getArrayValue('locality', $data),
      'postal_code'   => Utils::getArrayValue('postal_code', $data),
      'address_line1' => Utils::getArrayValue('address_line1', $data),
    ];
    foreach ($field_location as $key => $val) {
      if (!isset($accepted[$key])) {
        unset($field_location[$key]);
      }
    }
    $this->assertEquals($accepted, $field_location);

    $field_phone = array_map(function ($item) {
      return $item['value'];
    }, $location->get('field_phone')->getValue());
    $value = count($field_phone) == 1 ? $field_phone[0] : $field_phone;
    $this->assertEquals(Utils::getArrayValue('phone', $data), $value);

    $field_fax = array_map(function ($item) {
      return $item['value'];
    }, $location->get('field_fax')->getValue());
    $value = count($field_fax) == 1 ? $field_fax[0] : $field_fax;
    $this->assertEquals(Utils::getArrayValue('fax', $data), $value);

    $field_e_mail = array_map(function ($item) {
      return $item['value'];
    }, $location->get('field_e_mail')->getValue());
    $value = count($field_e_mail) == 1 ? $field_e_mail[0] : $field_e_mail;
    $this->assertEquals(Utils::getArrayValue('mail', $data), $value);
  }

}
