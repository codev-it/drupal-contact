<?php

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_contact.forms.inc
 * .
 */

use Drupal\Core\Field\FieldConfigBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_field_widget_form_alter().
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_contact_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  $items = $context['items'];
  $field_definition = $items->getFieldDefinition();
  if ($field_definition instanceof FieldConfigBase) {
    if ($field_definition->getTargetEntityTypeId() === 'paragraph'
      && $field_definition->getTargetBundle() == 'location'
      && $field_definition->get('field_name') == 'field_location') {
      $element['#type'] = 'container';
    }
  }
}
