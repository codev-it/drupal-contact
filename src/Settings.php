<?php

namespace Drupal\codev_contact;

use Drupal\codev_utils\SettingsBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * @package      Drupal\codev_contact
 *
 * @noinspection PhpUnused
 */
class Settings extends SettingsBase {

  /**
   * Module name.
   *
   * @var string
   */
  public const MODULE_NAME = 'codev_contact';

}
