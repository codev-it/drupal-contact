<?php

namespace Drupal\codev_contact\Plugin\Block;

use Drupal\codev_contact\Helper\Location as LocationHelper;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Location' block.
 *
 * @Block(
 *  id = "location",
 *  admin_label = @Translation("Location"),
 *  category = @Translation("Location")
 * )
 */
class Location extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->entityManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpMissingReturnTypeInspection
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'location';
    $build['#content'] = $this->buildRenderableArray();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['locations'] = [
      '#type'               => 'entity_autocomplete',
      '#title'              => $this->t('Locations'),
      '#description'        => $this->t('Choose which contact location to display.'),
      '#default_value'      => $this->loadNodes(),
      '#required'           => TRUE,
      '#target_type'        => 'node',
      '#tags'               => TRUE,
      '#selection_settings' => [
        'target_bundles' => ['contact'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $location = $form_state->getValue('locations');
    if (!empty($location)) {
      $this->configuration['locations'] = array_map(function ($item) {
        return $item['target_id'];
      }, $location);
    }
  }

  /**
   * Get all nodes they are having set show in location.
   *
   * @noinspection PhpMissingReturnTypeInspection
   */
  private function buildRenderableArray() {
    $ret = [];
    try {
      $locations = $this->getLocations();
      if (!empty($locations)) {
        foreach ($locations as $location) {
          $address = LocationHelper::buildCleanAddress($location);
          foreach (['phone', 'fax', 'mail'] as $item) {
            if (!empty($address[$item]) && !is_array($address[$item])) {
              $address[$item] = [$address[$item]];
            }
          }
          $ret[] = $address;
        }
      }
      return $ret;
    } catch (Exception $e) {
      return $ret;
    }
  }

  /**
   * Get all node entities.
   *
   * @return \Drupal\node\Entity\Node[]
   */
  function loadNodes(): array {
    try {
      $config = $this->getConfiguration();
      $ids = Utils::getArrayValue('locations', $config, []);
      $node_storage = $this->entityManager->getStorage('node');
      return $node_storage->loadMultiple($ids);
    } catch (Exception $exception) {
      return [];
    }
  }

  /**
   * Get all location from referenced nodes.
   *
   * @return array
   */
  private function getLocations(): array {
    $ret = [];
    foreach ($this->loadNodes() as $node) {
      if ($node->hasField('field_location')
        && !$node->get('field_location')->isEmpty()) {
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $location_field */
        $location_field = $node->get('field_location');
        foreach ($location_field->referencedEntities() as $entity) {
          $ret[] = $entity;
        }
      }
    }
    return $ret;
  }

}
