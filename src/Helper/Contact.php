<?php


namespace Drupal\codev_contact\Helper;


use Drupal;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Contact.php
 * .
 */

/**
 * Class Contact.
 *
 * @package      Drupal\codev_contact
 */
class Contact {

  /**
   * Create location paragraph entity.
   *
   * @param string                                     $title
   * @param array                                      $data
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *
   * @return Node|null
   */
  public static function createNode(string $title, array $data = [], ?AccountInterface $account = NULL): ?Node {
    try {
      $uid = $account instanceof AccountInterface ? $account->id() : 1;
      $body = !empty($data['body']) ? $data['body'] : '';
      $location = !empty($data['location']) ? $data['location'] : [];
      $menu_link = $data['menu_link'] ?? [];
      $menu_link_enable = $menu_link !== FALSE;
      $menu_link_title = !empty($menu_link['title']) ? $menu_link['title'] : $title;
      $menu_link_menu = !empty($menu_link['menu']) ? $menu_link['menu'] : 'main';

      // Create note entity
      /** @var \Drupal\node\NodeInterface $node */
      $node = Drupal::entityTypeManager()
        ->getStorage('node')
        ->create([
          'type'  => 'contact',
          'title' => $title,
          'body'  => [
            'value'  => $body,
            'format' => 'basic_html',
          ],
          'uid'   => $uid,
        ]);
      $node->save();

      // Add menu link to contact page
      if ($menu_link_enable) {
        $menu_link = Drupal::entityTypeManager()
          ->getStorage('menu_link_content')
          ->create([
            'title'     => $menu_link_title,
            'link'      => [
              'uri' => 'internal:/node/' . $node->id(),
            ],
            'menu_name' => $menu_link_menu,
            'expanded'  => TRUE,
            'weight'    => 99,
          ]);
        $menu_link->save();
      }

      // Set location
      if (!empty($location) && is_array($location)) {
        Location::setLocationToContactNode($node, $location);
      }

      return $node;
    } catch (Exception $exception) {
      return NULL;
    }
  }

}
