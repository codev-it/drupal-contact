<?php


namespace Drupal\codev_contact\Helper;


use Drupal;
use Drupal\codev_utils\Helper\Utils;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Location.php
 * .
 */

/**
 * Class Location.
 *
 * @package      Drupal\codev_contact
 */
class Location {

  /**
   * Default location paragraph type.
   */
  public const PARAGRAPH_TYPE = 'location';

  /**
   * Create location paragraph entity.
   *
   * @param array       $data
   * @param string|null $lang_code
   *
   * @return \Drupal\paragraphs\Entity\Paragraph|null
   */
  public static function createLocationParagraph(array $data = [], ?string $lang_code = NULL): ?Paragraph {
    try {
      $lang_code = $lang_code ?: Drupal::languageManager()
        ->getDefaultLanguage()->getId();
      $phone = Utils::getArrayValue('phone', $data);
      $fax = Utils::getArrayValue('fax', $data);
      $mail = Utils::getArrayValue('mail', $data);

      if (!empty($data['country_code'])) {
        $data['country_code'] = strtoupper($data['country_code']);
      }
      else {
        Drupal::logger('codev_contact')
          ->warning(t('Location paragraph cannot be created completely, mandatory field "country_core" is missing.'));
      }

      $excludes = ['phone', 'fax', 'email'];
      foreach ($excludes as $exclude) {
        if (!empty($data[$exclude])) {
          unset($data[$exclude]);
        }
      }

      /** @var Paragraph $paragraph_storage */
      $paragraph_storage = Drupal::entityTypeManager()
        ->getStorage('paragraph');
      return $paragraph_storage
        ->create([
          'type'           => static::PARAGRAPH_TYPE,
          'langcode'       => $lang_code,
          'field_location' => $data,
          'field_phone'    => $phone,
          'field_fax'      => $fax,
          'field_e_mail'   => $mail,
        ]);
    } catch (Exception $exception) {
      return NULL;
    }
  }

  /**
   * Set the location paragraph to current node.
   *
   * @param NodeInterface $node
   * @param array         $data
   * @param string|null   $lang_code
   * @param string        $field_name
   *
   * @return false
   */
  public static function setLocationToContactNode(NodeInterface $node, array $data = [], ?string $lang_code = NULL, string $field_name = 'field_location'): bool {
    if (!$node->hasField($field_name)) {
      return FALSE;
    }

    try {
      $field_value = static::buildTargetFieldValue($data, $lang_code);
      $node->set($field_name, $field_value);
      $node->save();
      return TRUE;
    } catch (Exception $exception) {
      return FALSE;
    }
  }

  /**
   * Append the location paragraph to current node.
   *
   * @param NodeInterface $node
   * @param array         $data
   * @param string|null   $lang_code
   * @param string        $field_name
   *
   * @return false
   *
   * @noinspection PhpUnused
   */
  public static function appendLocationToContactNode(NodeInterface $node, array $data = [], ?string $lang_code = NULL, string $field_name = 'field_location'): bool {
    if (!$node->hasField($field_name)) {
      return FALSE;
    }

    try {
      $field_value = static::buildTargetFieldValue($data, $lang_code);
      $current_value = $node->get($field_name)->getValue();
      $node->set($field_name, array_merge($current_value, $field_value));
      $node->save();
      return TRUE;
    } catch (Exception $exception) {
      return FALSE;
    }
  }

  /**
   * Load the location paragraph from current node.
   *
   * @param NodeInterface $node
   * @param string        $field_name
   *
   * @return array
   */
  public static function getLocationFromContactNode(NodeInterface $node, string $field_name = 'field_location'): array {
    try {
      $ret = [];
      /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $field */
      $field = $node->get($field_name);
      foreach ($field->referencedEntities() ?: [] as $item) {
        $ret[] = static::buildCleanAddress($item);
      }
      return $ret;
    } catch (Exception $exception) {
      return [];
    }
  }

  /**
   * Load the location paragraph from current node.
   *
   * @param int    $nid
   * @param string $field_name
   *
   * @return array
   *
   * @noinspection PhpUnused
   */
  public static function getLocationFromContactNodeById(int $nid, string $field_name = 'field_location'): array {
    try {
      /** @var \Drupal\node\Entity\Node $node */
      $node = Drupal::entityTypeManager()
        ->getStorage('node')
        ->load($nid);
      return static::getLocationFromContactNode($node, $field_name);
    } catch (Exception $exception) {
      return [];
    }
  }

  /**
   * Build clean address array from location paragraph entity.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *
   * @return array
   */
  public static function buildCleanAddress(ParagraphInterface $paragraph): array {
    $value = function ($items) {
      foreach ($items as &$item) {
        $item = $item['value'];
      }
      return $items;
    };
    $ret = !$paragraph->get('field_location')->isEmpty()
      ? $paragraph->get('field_location')->getValue()[0] : [];
    $ret['phone'] = $value($paragraph->get('field_phone')->getValue() ?: []);
    $ret['fax'] = $value($paragraph->get('field_fax')->getValue() ?: []);
    $ret['mail'] = $value($paragraph->get('field_e_mail')->getValue() ?: []);
    return $ret;
  }

  /**
   * Build the paragraph target field value.
   *
   * @param             $data
   * @param string|null $lang_code
   *
   * @return array
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function buildTargetFieldValue($data, ?string $lang_code = NULL): array {
    $ret = [];
    if (Utils::isArrayAssoc($data)) {
      if ($paragraph = static::createLocationParagraph($data, $lang_code)) {
        $paragraph->save();
        $ret[] = [
          'target_id'          => $paragraph->id(),
          'target_revision_id' => $paragraph->getRevisionId(),
        ];
      }
    }
    else {
      foreach ($data as $item) {
        if ($paragraph = static::createLocationParagraph($item, $lang_code)) {
          $paragraph->save();
          $ret[] = [
            'target_id'          => $paragraph->id(),
            'target_revision_id' => $paragraph->getRevisionId(),
          ];
        }
      }
    }
    return $ret;
  }

}
